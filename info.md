# Installation

## Create virtual environment

> On linux
>
> > `python3 -m venv venv`
>
> On windows
>
> > `py -3 -m venv venv` or `python -m venv venv`

## Activate virtual environment

> On linux
>
> > `source ./venv/bin/activate`
>
> On windows
>
> > `venv\Scripts\activate.bat`
> 
> On Visual Studio Code
>
> > `crl + shift + p`
> 
> > `Type Python: Select Interpreter`
> 
> > `Select virtual environment Interpreter`

# Development mode and FLASK_APP env var

## Windows

> Set development mode:
>
> > `set FLASK_ENV=development`

> Set the FLASK_APP environment variable:
>
> > `set FLASK_APP=run.py`
>
> > `set FLASK_DEBUG=1.`

## Linux

> Set development mode:
>
> > `export FLASK_ENV=development`

> Set the FLASK_APP environment variable
>
> > `export FLASK_APP=run.py`

# Run the app

> `flask run`

> 