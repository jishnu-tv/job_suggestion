$(document).ready(function () {
  $(".datepicker").datepicker();
  $("select").formSelect();
  $('.modal').modal();
  $('.tabs').tabs();
  $('.sidenav').sidenav();
	$('#sidenav-1').sidenav({ edge: 'left' });
	$('#sidenav-2').sidenav({ edge: 'right' });
  AOS.init();
});

function numberRows($t) {
  var c = 0;
  $t.find("tr").each(function(ind, el) {
    $(el).find("td:eq(0)").html(++c);
  });
}

numberRows($("#applied_tb"));

$(".dropdown-trigger").dropdown({
  coverTrigger: true,
  // hover: true
});

   