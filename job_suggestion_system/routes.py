import time
import os
from flask import render_template, request, redirect, flash, url_for, session
from job_suggestion_system import app, db, bcrypt, images, files
from job_suggestion_system.forms import EmployeeLoginForm, EmployerLoginForm, EmployeeRegisterForm, EmployerRegisterForm, JobPostForm, AdminLoginForm, SearchForm
from job_suggestion_system.models import Employee, Employer, Jobs, Admin, applied
from flask_login import login_user, current_user, logout_user


# Home
@app.route("/",  methods=["GET", "POST"])
def home():
    jobs = Jobs.query.all()
    form = SearchForm()
    if form.validate_on_submit():
        return redirect(url_for('search', name=form.search.data))
    return render_template('index.html', title="Job suggestion system", jobs=jobs, form=form)


# Search
@app.route("/search/<name>")
def search(name):
    val = name
    jobs = Jobs.query.filter(Jobs.job_title.like('%' + val + '%'))
    jobs = jobs.order_by(Jobs.job_title).all()
    return render_template('search.html', title="Search", cls="active", val=val, jobs=jobs)


# About
@app.route("/about")
def about():
    return render_template('about.html', title="About", cls="active")


# Contact
@app.route("/contact")
def contact():
    return render_template('contact.html', title="Contact Us", cls="active")


# Employee Dashboard
@app.route("/employee")
def employee():
    if current_user.is_authenticated and current_user.role == 'employee':
        job_id = 1
        jobs = Jobs.query.filter_by(id=job_id).first()
        return render_template('employee/index.html', title="Dashboard")
    else:
        return redirect(url_for('home'))


# Login employee
@app.route("/login/employee", methods=["GET", "POST"])
def loginEmployee():
    form = EmployeeLoginForm()
    if current_user.is_authenticated:
        u = current_user.role
        return redirect(url_for(u))
    if form.validate_on_submit():
        session.pop('role', None)
        employee = Employee.query.filter_by(email=form.email.data).first()
        if employee and bcrypt.check_password_hash(employee.password, form.password.data):
            session['role'] = employee.role
            login_user(employee, remember=form.rememberMe.data)
            return redirect(url_for('employee'))
        else:
            flash(f'Login failed. Email or password is incorrect')
            return redirect(url_for('loginEmployee'))
        return redirect(url_for('employee Dashboard'))
    return render_template('employee/login-employee.html', title="Employee login", cls="active", form=form)


# Register Employee
@app.route("/register/employee", methods=["GET", "POST"])
def registerEmployee():
    form = EmployeeRegisterForm()
    if current_user.is_authenticated:
        u = current_user.role
        return redirect(url_for(u))
    if form.validate_on_submit():
        timestr = time.strftime("%Y%m%d_%H%M%S")

        image = request.files['photo']
        cv_file = request.files['cv']

        file_name, file_extension = os.path.splitext(image.filename)
        image.filename = str(timestr+"_"+form.email.data+file_extension)

        ext = os.path.splitext(cv_file.filename)
        file_ext = ext[1]
        cv_file.filename = str(timestr+"_"+form.email.data+file_ext)

        cv = files.save(form.cv.data)
        photo = images.save(form.photo.data)
        psw_hash = bcrypt.generate_password_hash(
            form.password.data).decode("utf-8")
        employee = Employee(firstname=form.firstname.data, lastname=form.lastname.data,
                            email=form.email.data,
                            phone=form.phone.data, house=form.house.data,
                            city=form.city.data, state=form.state.data,
                            pin=form.pin.data, photo=photo, cv=cv, dob=form.dob.data,
                            qualification=form.qualification.data,
                            password=psw_hash)
        db.session.add(employee)
        db.session.commit()
        flash('You are successfully registered. You can now login')
        return redirect(url_for('loginEmployee'))
    return render_template('employee/register-employee.html', title="Employee register",  cls="active", form=form)


# Employer Dashboard
@app.route("/employer")
def employer():
    if current_user.is_authenticated and current_user.role == 'employer':
        jobs = current_user.jobs
        return render_template('employer/index.html', title="Dashboard", jobs=jobs)
    else:
        return redirect(url_for('home'))


# Login employer
@app.route("/login/employer", methods=["GET", "POST"])
def loginEmployer():
    form = EmployerLoginForm()
    if current_user.is_authenticated:
        u = current_user.role
        return redirect(url_for(u))
    if form.validate_on_submit():
        session.pop('role', None)
        employer = Employer.query.filter_by(
            email=form.employer_email.data).first()
        if employer and bcrypt.check_password_hash(employer.password, form.password.data):
            session['role'] = employer.role
            login_user(employer, remember=form.rememberMe.data)
            return redirect(url_for('employer'))
        else:
            flash(f'Login failed. Email or password is incorrect')
            return redirect(url_for('loginEmployer'))
        return redirect(url_for('employerDashboard'))
    return render_template('employer/login-employer.html', title="Employer login", cls="active", form=form)


# Register Employer
@app.route("/register/employer", methods=["GET", "POST"])
def registerEmployer():
    form = EmployerRegisterForm()
    if current_user.is_authenticated:
        u = current_user.role
        return redirect(url_for(u))
    if form.validate_on_submit():
        timestr = time.strftime("%Y%m%d_%H%M%S")
        file = request.files['photo']
        file_name, file_extension = os.path.splitext(file.filename)
        file.filename = str(timestr+"_"+form.email.data+file_extension)
        photo = images.save(form.photo.data)
        psw_hash = bcrypt.generate_password_hash(
            form.pswd.data).decode("utf-8")
        employer = Employer(first_name=form.first_name.data, last_name=form.last_name.data,
                            company_name=form.company_name.data, email=form.email.data,
                            phone=form.phone.data, about=form.about.data,
                            street=form.street.data, address_2=form.address_2.data,
                            city=form.city.data, state=form.state.data, country=form.country.data,
                            zipcode=form.zipcode.data, photo=photo, password=psw_hash)
        db.session.add(employer)
        db.session.commit()
        flash('You are successfully registered. You can now login')
        return redirect(url_for('loginEmployer'))
    return render_template('employer/register-employer.html', title="Employer register",  cls="active", form=form)


# Post Job
@app.route("/employer/post-job", methods=["GET", "POST"])
def postJob():
    form = JobPostForm()
    if not current_user.is_authenticated:
        return redirect(url_for('home'))
    if not current_user.role == 'employer':
        return redirect(url_for('home'))
    if form.validate_on_submit():
        employerid = current_user.id
        timestr = time.strftime("%Y%m%d_%H%M%S")
        file = request.files['photo']
        file_name, file_extension = os.path.splitext(file.filename)
        file.filename = str(timestr+file_extension)
        photo = images.save(form.photo.data)
        post_job = Jobs(employer_id=employerid, job_title=form.employer_job_title.data, job_tags=form.employer_job_tags.data,
                        job_qualification=form.employer_job_qualification.data, job_type=form.employer_job_type.data,
                        job_experience=form.employer_job_experience.data, job_min_salary=form.employer_job_min_salary.data,
                        job_max_salary=form.employer_job_max_salary.data, photo=photo, face_to_face=form.employer_face_to_face.data,
                        writtentest=form.employer_writtentest.data, telephonic=form.employer_telephonic.data, group_discussion=form.employer_group_discussion.data,
                        walk_in=form.employer_walk_in.data, job_location=form.employer_job_location.data,
                        job_country=form.employer_job_country.data, job_description=form.employer_job_description.data)
        db.session.add(post_job)
        db.session.commit()
        flash('You are successfully posted')
        return redirect(url_for('employer'))
    return render_template('employer/post-job.html', title="Employer ",  cls="active", form=form)


# Delete Job
@app.route("/employer/delete-job/<id>")
def deleteJob(id):
    job_id = id
    if current_user.is_authenticated and current_user.role == 'employer':
        job = Jobs.query.filter_by(id=job_id).first()
        os.remove(os.path.join(app.config['UPLOADED_PHOTOS_DEST'], job.photo))
        db.session.delete(job)
        db.session.commit()
        flash('Job deleted')
        return redirect(url_for('employer'))
    else:
        return redirect(url_for('home'))


# Admin login
@app.route("/login/admin", methods=["GET", "POST"])
def adminLogin():
    form = AdminLoginForm()
    if current_user.is_authenticated:
        u = current_user.role
        return redirect(url_for(u))
    if form.validate_on_submit():
        session.pop('role', None)
        admin = Admin.query.filter_by(username=form.username.data).first()
        if admin and bcrypt.check_password_hash(admin.password, form.password.data):
            session['role'] = admin.role
            login_user(admin)
            return redirect(url_for('admin'))
        else:
            flash(f'Login failed. username or password is incorrect')
            return redirect(url_for('adminLogin'))
        return redirect(url_for('admin'))
    return render_template('admin/admin-login.html', title="Admin login", cls="active", form=form)


# Admin
@app.route("/admin")
def admin():
    employers = Employer.query.all()
    employees = Employee.query.all()
    if current_user.is_authenticated and current_user.role == 'admin':
        return render_template('admin/index.html', title="Dashboard", employers=employers, employees=employees)
    else:
        return redirect(url_for('home'))


# Admin employee manage
@app.route("/admin/employee")
def manageEmployee():
    employees = Employee.query.all()
    if current_user.is_authenticated and current_user.role == 'admin':
        return render_template('admin/manage/employee.html', title="Dashboard", employees=employees)
    else:
        return redirect(url_for('home'))


# Admin employer manage
@app.route("/admin/employer")
def manageEmployer():
    employers = Employer.query.all()
    if current_user.is_authenticated and current_user.role == 'admin':
        return render_template('admin/manage/employer.html', title="Dashboard", employers=employers)
    else:
        return redirect(url_for('home'))


# Admin employee-details manage
@app.route("/admin/employee/<id>")
def viewEmployee(id):
    employee_id = id
    employee = Employee.query.filter_by(id=employee_id).first()
    if current_user.is_authenticated and current_user.role == 'admin':
        return render_template('admin/manage/employee-details.html', title="Dashboard", employee=employee)
    else:
        return redirect(url_for('home'))


# Delete Employee
@app.route("/delete-employee/<id>")
def deleteEmployee(id):
    employee_id = id
    if current_user.is_authenticated and current_user.role == 'admin':
        emp = Employee.query.filter_by(id=employee_id).first()
        os.remove(os.path.join(app.config['UPLOADED_PHOTOS_DEST'], emp.photo))
        os.remove(os.path.join(app.config['UPLOADED_FILES_DEST'], emp.cv))
        db.session.delete(emp)
        db.session.commit()
        return redirect(url_for('manageEmployee'))
    else:
        return redirect(url_for('home'))


# Admin employer-details manage
@app.route("/admin/employer/<id>")
def viewEmployer(id):
    emp_id = id
    employer = Employer.query.filter_by(id=emp_id).first()
    if current_user.is_authenticated and current_user.role == 'admin':
        return render_template('admin/manage/employer-details.html', title="Dashboard", employer=employer)
    else:
        return redirect(url_for('home'))


# Delete Employer
@app.route("/delete-employer/<id>")
def deleteEmployer(id):
    employer_id = id
    if current_user.is_authenticated and current_user.role == 'admin':
        emp = Employer.query.filter_by(id=employer_id).first()
        jobs = Jobs.query.filter_by(employer_id=employer_id)
        for j in jobs:
            os.remove(os.path.join(
                app.config['UPLOADED_PHOTOS_DEST'], j.photo))
        os.remove(os.path.join(app.config['UPLOADED_PHOTOS_DEST'], emp.photo))
        jobs.delete()
        db.session.delete(emp)
        db.session.commit()
        return redirect(url_for('manageEmployer'))
    else:
        return redirect(url_for('home'))


# Apply Job
@app.route("/apply-job/<job_id>")
def appplyJob(job_id):
    if current_user.is_authenticated and current_user.role == 'employee':
        job = Jobs.query.filter_by(id=job_id).first()
        print(job.applied_user)
        job.applied_user.append(current_user)
        db.session.commit()
        flash('You are successfully applied')
        return redirect(url_for('employee'))
    else:
        flash('Please login as employee')
        return redirect(url_for('home'))


@app.route("/not-login-employee")
def notLoggedinEmployee():
    flash('Please login to continue')
    return redirect(url_for('loginEmployee'))


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))
