from job_suggestion_system import db, login_manager
from flask_login import UserMixin
from flask import session


@login_manager.user_loader
def get_user(user_id):
    role = session.get('role')
    if role == 'admin':
        return Admin.query.get(int(user_id))
    elif role == 'employee':
        return Employee.query.get(int(user_id))
    elif role == 'employer':
        return Employer.query.get(int(user_id))


applied = db.Table('applied',
                   db.Column('employee_id', db.Integer,
                             db.ForeignKey('employee.id')),
                   db.Column('job_id', db.Integer, db.ForeignKey('jobs.id'))
                   )


class Admin(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), nullable=False)
    password = db.Column(db.String(120), nullable=False)
    role = db.Column(db.String(8), default="admin")


class Employee(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(120), nullable=False)
    lastname = db.Column(db.String(120), nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    phone = db.Column(db.String(120), unique=True, nullable=False)
    house = db.Column(db.String(120), nullable=False)
    city = db.Column(db.String(120), nullable=False)
    state = db.Column(db.String(120), nullable=False)
    pin = db.Column(db.String(6), nullable=False)
    dob = db.Column(db.String(120), nullable=False)
    qualification = db.Column(db.String(120), nullable=False)
    photo = db.Column(db.String(120), default="default-user.jpg")
    cv = db.Column(db.String(120))
    password = db.Column(db.String(120), nullable=False)
    role = db.Column(db.String(8), default="employee")
    jobs = db.relationship('Jobs', secondary=applied, backref=db.backref(
        'applied_user', lazy=True, uselist=True))


class Employer(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(30), nullable=False)
    last_name = db.Column(db.String(30),  nullable=False)
    company_name = db.Column(db.String(120), nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    phone = db.Column(db.String(10), unique=True, nullable=False)
    about = db.Column(db.String(256),  nullable=False)
    street = db.Column(db.String(120),  nullable=False)
    address_2 = db.Column(db.String(120),  nullable=False)
    city = db.Column(db.String(50),  nullable=False)
    state = db.Column(db.String(50),  nullable=False)
    country = db.Column(db.String(50), nullable=False)
    zipcode = db.Column(db.String(6), nullable=False)
    photo = db.Column(db.String(120), default="default-user.jpg")
    password = db.Column(db.String(120), nullable=False)
    role = db.Column(db.String(8), default="employer")
    jobs = db.relationship('Jobs', backref='job', lazy=True)


class Jobs(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    employer_id = db.Column(db.Integer, db.ForeignKey(
        'employer.id'), nullable=False)
    job_title = db.Column(db.String(120),  nullable=False)
    job_tags = db.Column(db.String(120),  nullable=False)
    job_qualification = db.Column(db.String(120),  nullable=False)
    job_type = db.Column(db.String(120), nullable=False)
    job_experience = db.Column(db.String(120), nullable=False)
    job_min_salary = db.Column(db.String(120), nullable=False)
    job_max_salary = db.Column(db.String(120), nullable=False)
    face_to_face = db.Column(db.Boolean, default=False)
    writtentest = db.Column(db.Boolean, default=False)
    telephonic = db.Column(db.Boolean, default=False)
    group_discussion = db.Column(db.Boolean, default=False)
    walk_in = db.Column(db.Boolean, default=False)
    job_location = db.Column(db.String(120), nullable=False)
    job_country = db.Column(db.String(120), nullable=False)
    photo = db.Column(db.String(120), default="default-job.jpg")
    job_description = db.Column(db.String(120), nullable=False)
