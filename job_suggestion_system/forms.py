from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, IntegerField, TextAreaField, SelectField, FileField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Email, Length, EqualTo


# Admin Login Form
class AdminLoginForm(FlaskForm):
    username = StringField("user name", validators=[
        DataRequired("user name is required")])
    password = PasswordField("Password", validators=[DataRequired(
        "Password is required"), Length(min=8, max=20, message="Minimum password length 8 char")])


# Employee Login Form
class EmployeeLoginForm(FlaskForm):
    email = EmailField("Email address", validators=[DataRequired(
        "Email is required"), Email("Email address is invalid")])
    password = PasswordField("Password", validators=[DataRequired(
        "Password is required"), Length(min=8, max=20, message="Minimum password length 8 char")])
    rememberMe = BooleanField("Remember me")


# Employer Login Form
class EmployerLoginForm(FlaskForm):
    employer_email = EmailField("Email", validators=[DataRequired(
        "Email is required"), Email("Email address is invalid")])
    password = PasswordField("Password", validators=[DataRequired(
        "Password is required"), Length(min=8, max=20, message="Minimum password length 8 char")])
    rememberMe = BooleanField("Remember me")


# Employee Registration Form
class EmployeeRegisterForm(FlaskForm):
    firstname = StringField("First Name", validators=[DataRequired("First Name is required"), Length(
        min=2, message=" First Name must be atleast 2 charactrs long")])
    lastname = StringField("Last Name", validators=[
                           DataRequired("Last Name is required")])
    email = EmailField("Email", validators=[DataRequired(
        "Email is required"), Email("Email address is invalid")])
    phone = StringField("Phone", validators=[
                        DataRequired("Phone number is required")])
    house = StringField("House", validators=[
                        DataRequired("House name is required")])
    city = StringField("City", validators=[
                       DataRequired("City name is required")])
    state = StringField("State", validators=[
                        DataRequired("State name is required")])
    pin = IntegerField("Pin", validators=[
                       DataRequired("Pin number is required")])
    dob = StringField("Date of Birth", validators=[
                      DataRequired("Date of birth is required")])
    qualification = StringField("Qualification", validators=[
                                DataRequired("Qualification is required")])
    photo = FileField('Profile image')

    cv = FileField('Upload cv')

    password = PasswordField("Password", validators=[DataRequired(
        "Password is required"), Length(min=8, max=20, message="Minimum password length 8 char")])
    cpassword = PasswordField("Confirm Password", validators=[DataRequired(
        "Confirm Password is required"), EqualTo('password', message='Passwords must match')])


# Employer Registration Form
class EmployerRegisterForm(FlaskForm):
    first_name = StringField("First Name", validators=[DataRequired(
        "First name is required"), Length(min=2, message=" First name must be atleast 2 charactrs long")])
    last_name = StringField("Last Name", validators=[DataRequired(
        "Last name is required"), Length(min=2, message="Last name must be atleast 2 charactrs long")])
    company_name = StringField("Company Name", validators=[DataRequired(
        "Company name is required"), Length(min=2, message="Company name must be atleast 2 charactrs long")])
    email = EmailField("Email", validators=[DataRequired(
        "Email is required"), Email("Email address is invalid")])
    phone = StringField(
        "Phone", validators=[DataRequired("Phone number is required")])
    about = TextAreaField(
        "About", validators=[DataRequired("About is required")])
    street = StringField(
        "Street address", validators=[DataRequired("Street address is required")])
    address_2 = StringField("Address line 2")
    city = StringField(
        "City", validators=[DataRequired("City name is required")])
    state = StringField(
        "State", validators=[DataRequired("State is required")])
    country = StringField(
        "Country", validators=[DataRequired("Country is required")])
    zipcode = StringField(
        "Zip code", validators=[DataRequired("Zip code is required")])
    photo = FileField('Profile image')
    pswd = PasswordField("Password", validators=[DataRequired(
        "Password is required"), Length(min=8, max=20, message="Minimum password length 8 char")])
    pswd2 = PasswordField("Confirm Password", validators=[DataRequired(
        "Confirm Password is required"), EqualTo('pswd', message="Passwords must match")])


# Post Job Form
class JobPostForm(FlaskForm):
    employer_job_title = StringField("Job Title", validators=[
                                     DataRequired("Job Title is required")])
    employer_job_tags = StringField(
        "Job Tags", validators=[DataRequired("Job Tag is required")])
    employer_job_qualification = StringField("Job Qualification", validators=[
                                             DataRequired("Qualification is required")])
    employer_job_type = SelectField("Job Type", choices=[("", "Select Job Type"), ("Fulltime", "Fulltime"), ("Parttime", "Parttime"), (
        "Internship", "Internship"), ("Frelance", "Frelance")], validators=[DataRequired("Job Type is required")])
    employer_job_experience = SelectField("Experience", choices=[("", "Select Experience"), ("0", "Fresher"), ("1", "1 Years"), ("2", "2 years"), (
        "3", "3 Years"), ("4", "4 Years"), ("5", "5 Years"), ("5+", "5+ Years")], validators=[DataRequired("Job Type is required")])
    employer_job_min_salary = StringField("Minimum Salary", validators=[
                                          DataRequired("Minimum salary is required")])
    employer_job_max_salary = StringField("Maximum Salary", validators=[
                                          DataRequired("Maximum salary is required")])
    photo = FileField('Profile image')
    employer_face_to_face = BooleanField("Face to  Face")
    employer_writtentest = BooleanField("Written-Test")
    employer_telephonic = BooleanField("Telephonic")
    employer_group_discussion = BooleanField("Group Discussion")
    employer_walk_in = BooleanField("Walk In")
    employer_job_location = StringField("Job location", validators=[
                                        DataRequired("Job location is required")])
    employer_job_country = StringField(
        "country", validators=[DataRequired("country is required")])
    photo = FileField('Job image')
    employer_job_description = StringField(
        "description", validators=[DataRequired("job description is required")])

# Job Search


class SearchForm(FlaskForm):
    search = StringField("Search jobs")
